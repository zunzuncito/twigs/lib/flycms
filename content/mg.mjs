
import fs from 'fs'
import path from 'path'

import YAML from 'yaml'

import FlyModule from 'zunzun/flymodule/index.mjs'
import ContentModel from "./model.mjs"

import ObjectUtil from 'zunzun/flyutil/object.mjs'

const PERMISSIONS = [
  "public", "private", "admin"
]

const METHODS = [
  "get", "post", "put", "delete"
]

export default class ContentManager {
  constructor(flycms_path, srv_flycms_paths, cfg) {

    this.path = flycms_path;
    this.models_path = path.resolve(this.path, "models");
    this.api_hooks_path = path.resolve(this.path, "api");
    this.albums_path = path.resolve(this.path, "albums");
    this.lang_path = path.resolve(this.path, "lang");

    this.available_languages = cfg.available_languages;

    this.srv_paths = srv_flycms_paths;
    this.srv_models_paths = [];
    for (let srv_path of this.srv_paths) {
      const srv_models_path = path.join(srv_path, "models");
      if (fs.existsSync(srv_models_path)) this.srv_models_paths.push(srv_models_path);
    }

    this.models = {};
    this.albums = {};

    this.load_models(this.models_path);
  }

  load_lang(srv_model_file, model_cfg) {
    const srv_model_lang_path = path.join(this.lang_path, "models", srv_model_file.slice(0, -5));

    if (fs.existsSync(srv_model_lang_path)) {
      for (let alang of this.available_languages) {
        const lang_path = path.join(srv_model_lang_path, alang+".json");
        if (fs.existsSync(lang_path)) {
          const model_lang = JSON.parse(fs.readFileSync(lang_path, 'utf8'));
          if (typeof model_cfg.name != "object") model_cfg.name = {};
          model_cfg.name[alang] = model_lang.name;
          for (let col in model_lang.columns) {
            for (let c = 0; c < model_cfg.columns.length; c++) {
              if (model_cfg.columns[c].name == col) {
                if (typeof model_cfg.columns[c].title != "object") model_cfg.columns[c].title = {};
                model_cfg.columns[c].title[alang] = model_lang.columns[col];
              }
            }
          }
        }
      }
    }
  }

  load_models(models_path) {
    const model_cfgs = {};
    for (let srv_models_path of this.srv_models_paths) {
      const srv_model_files = fs.readdirSync(srv_models_path);
      for (let srv_model_file of srv_model_files) {
        const srv_model_file_path = path.resolve(srv_models_path, srv_model_file);
        const model_cfg = YAML.parse(fs.readFileSync(srv_model_file_path, "utf8"));

        this.load_lang(srv_model_file, model_cfg);


        if (!model_cfgs[model_cfg.api_name]) {
          model_cfgs[model_cfg.api_name] = model_cfg;
        } else {
          model_cfgs[model_cfg.api_name] = ObjectUtil.force_add(
            model_cfgs[model_cfg.api_name],
            model_cfg
          );
        }

      }
    }

    if (!fs.existsSync(models_path)) {
      console.error(new Error(`Invalid path for cms models. Directory ${models_path} does not exist!`));
    }

    const model_files = fs.readdirSync(models_path);
    for (let model_file of model_files) {
      const model_file_path = path.resolve(models_path, model_file);
      const model_cfg = YAML.parse(fs.readFileSync(model_file_path, "utf8"));

      this.load_lang(model_file, model_cfg);
      if (!model_cfgs[model_cfg.api_name]) {
        model_cfgs[model_cfg.api_name] = model_cfg;
      } else {
        model_cfgs[model_cfg.api_name] = ObjectUtil.force_add(
          model_cfgs[model_cfg.api_name],
          model_cfg
        );
      }
    }

    for (let model_cfg in model_cfgs) {
      const model = new ContentModel(model_cfgs[model_cfg]);
      this.models[model.api_name] = model;
    }
  }

  async load_api_hooks() {
    const hooks_path = this.api_hooks_path;
    if (!fs.existsSync(this.api_hooks_path)) throw new Error(`API hooks directory ${this.api_hooks_path} does not exist!`);
    const hook_dirs = fs.readdirSync(hooks_path);

    for (let hdir of hook_dirs) {
      const hdir_path = path.resolve(hooks_path, hdir);

      for (let model_tag in this.models) {
        const model = this.models[model_tag];


        if (model_tag == hdir) {
          model.hooks = {};

          const hook_dir_permissions = fs.readdirSync(hdir_path);
           
          for (let perm_dir of hook_dir_permissions) {
            if (PERMISSIONS.includes(perm_dir)) {

              if (!model.api[perm_dir]) model.api[perm_dir] = {};

              for (let method of METHODS) { 
                let hook_method_path = path.resolve(hooks_path, model_tag , perm_dir, `${method}.mjs`);
                if (fs.existsSync(hook_method_path)) {
                  if (!model.api[perm_dir][method]) model.api[perm_dir][method] = {};
                  model.api[perm_dir][method].hooks = (await FlyModule.load(path.dirname(hook_method_path), path.basename(hook_method_path), true)).default;
                }

              }

            } else {
              console.error(new Error(`Error: Invalid flycms api permission directory: "${perm_dir}"`));
            }
          }
        }
      }
    }
  }
}
