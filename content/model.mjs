
import fs from 'fs'
import path from 'path'


import QueryParser from '../query/parser.mjs'

export default class ContentModel {

  constructor(cfg) {
    console.debug("Model:", cfg.api_name);
    for (let key in cfg) {
      this[key] = cfg[key];
    }

  }

  async construct(pg) {
    try {
      this.pg = pg;

      const columns = this.columns;
      const pg_columns = {
        id: this.uuid ? "uuid PRIMARY KEY DEFAULT gen_random_uuid()" : "serial PRIMARY KEY"
      };

      let has_media = false;
      for (let column of this.columns) {
        const varchar_match = column.type.match(/^varchar\((\d+)\)$/);
        if (varchar_match) {
          pg_columns[column.name] = column.type;
          column.type = 'text';
          column.max = parseInt(varchar_match[1]);
        } else {
          switch (column.type) {
            case 'uuid':
              pg_columns[column.name] = 'uuid';
              if (column.references) {
                pg_columns[column.name] += ` REFERENCES ${column.references}(id)`
                if (column.cascade) {
                   pg_columns[column.name] += ` ON DELETE CASCADE`;
                }
              }
              column.pg_type = pg_columns[column.name];
              column.type = 'text';
              break;
            case 'text':
              pg_columns[column.name] =  column.lang ? 'jsonb' : 'text';
              if (column.lang) this.lang = true;
              if (column.rich) this.has_media = true;
              break;
            case 'text[]':
              pg_columns[column.name] = 'text[]';
              column.pg_type = pg_columns[column.name];
              column.type = 'text';
              break;
            case 'int':
              pg_columns[column.name] = 'bigint';
              column.pg_type = pg_columns[column.name];
              break;
            case 'smallint':
              pg_columns[column.name] = 'smallint';
              column.pg_type = pg_columns[column.name];
              column.type = 'int';
              break;
            case 'smallint[]':
              pg_columns[column.name] = 'smallint[]';
              column.pg_type = pg_columns[column.name];
              column.type = 'int';
              break;
            case 'bigint':
              pg_columns[column.name] = 'bigint';
              column.pg_type = pg_columns[column.name];
              column.type = 'int';
              break;
            case 'bigint[]':
              pg_columns[column.name] = 'bigint[]';
              column.pg_type = pg_columns[column.name];
              column.type = 'int';
              break;
            case 'float':
              pg_columns[column.name] = 'float';
              column.pg_type = pg_columns[column.name];
              break;
            case 'media':
              pg_columns[column.name] = 'text';
              if (column.multifile) pg_columns[column.name] += '[]';
              column.pg_type = pg_columns[column.name];
              this.has_media = true;
              break;
            case 'boolean':
              pg_columns[column.name] = 'boolean';
              column.pg_type = pg_columns[column.name];
              break;
            case 'date-time':
              pg_columns[column.name] = 'timestamp';
              column.pg_type = pg_columns[column.name];
              break;
            case 'date':
              pg_columns[column.name] = 'date';
              column.pg_type = pg_columns[column.name];
              break;
            case 'time':
              pg_columns[column.name] = 'time';
              column.pg_type = pg_columns[column.name];
              break;
            default:
              console.error(new Error(`Invalid model column type: ${column.type}`));
          };
        }
      }

      if (!this.table_name) this.table_name = `flycms_table_${this.api_name}`;

      this.table = await pg.table(this.table_name, pg_columns);

    } catch (e) {
      console.error(e.stack);
    }
  }

  get_column(name) {
    for (let column of this.columns) {
      if (column.name == name) return column;
    }
  }

  stack_to_aura(exprs, layer, cfg) {


    if (!layer) layer = 0;

    let psql_condition = '';
    let psql_vals = [];

    if (layer > 0) {
      psql_condition += "("
    }

    for (let expr of exprs) {
      if (expr === "|") {
        psql_condition += " OR ";
      } else if (expr === "&") {
        psql_condition += " AND ";
      } else if (Array.isArray(expr)) {
        const [nested_condition, nested_vals] = this.stack_to_aura(expr, layer+1);
        psql_condition += nested_condition;
        psql_vals = [...psql_vals, ...nested_vals];
      } else if (typeof expr == "object") {
        if (expr.vals) {
          let condition_left = '';
          console.log("cfg.lang", cfg.lang);
          if (cfg.lang) {
            condition_left += `${expr.column}->>'${cfg.lang}' `;
          } else {
            condition_left += `${expr.column} `;
          }

          if (expr.lower) {
            condition_left = `LOWER(${condition_left}) `;
          }
          
          psql_condition += `${condition_left}${expr.operator} `;


          let cindex = expr.val_index;

          if (expr.any) {
            psql_condition += `ANY($${cindex})`;
            cindex++;
          } else {
            psql_condition += `$${cindex}`;
          }

          psql_vals.push(expr.vals);

/*          if (expr.any) psql_condition += `ANY(ARRAY[`;
          let first = true;
          for (let val of expr.vals) {
            if (!first) {
              psql_condition += `,`;
            } else {
              first = false;
            }
            psql_condition += `$${cindex}`;
            psql_vals.push(val);
            cindex++;
          }
          if (expr.any) psql_condition += `]::text[])`;*/
        } else {

          if (expr.any) {
            psql_condition += `${expr.column} ${expr.operator} ANY($${expr.val_index})`;
          } else {
            psql_condition += `${expr.column} ${expr.operator} $${expr.val_index}`;
          }
          psql_vals.push(expr.value);
        }
      }
    }

    if (layer > 0) {
      psql_condition += ")"
    }
    
    return [psql_condition, psql_vals];
  }

  async select(columns, cfg) {
    if (!columns) columns = "*";
    let items = undefined;

    let where_expr = undefined;
    let where_vals = undefined;

    if (cfg && typeof cfg.where == "string") {
      console.log("cfg.where", cfg.where);
      cfg.where = QueryParser.conditions(cfg.where);
    }

    if (cfg && cfg.where) {
      const aura_where = this.stack_to_aura(cfg.where, 0, cfg);
      where_expr = aura_where[0];
      where_vals = aura_where[1];

      /*
      const expri = 1;
      where_vals = [];
      for (let col in cfg.where) {
        if (typeof where_expr == "string") {
          where_expr += " AND ";
        } else {
          where_expr = "";
        }
        where_expr += `${col} = $${expri}`;
        where_vals.push(cfg.where[col]);
      }*/
    }


    let additional_query = ``;
    if (cfg) {
      if (typeof cfg.order == "string") {
        const col = this.get_column(cfg.order);
        if (col && col.type == "jsonb" && cfg.lang) {
          additional_query += `ORDER BY ${cfg.order}->>'${cfg.lang}'`;
        } else {
          additional_query += `ORDER BY ${cfg.order}`;
        }
      } else {
        additional_query += `ORDER BY id`;
      }

      console.log("cfg.desc", cfg.desc);

      if (cfg.desc) {
        additional_query += ` DESC`;
      } else {
        additional_query += ` ASC`;
      }

      if (typeof cfg.offset == "number") additional_query += ` OFFSET ${cfg.offset}`;
      if (typeof cfg.limit == "number") additional_query += ` LIMIT ${cfg.limit}`;
    }

    if (
      where_expr && where_vals
    ) {
      console.log("MODEL_SELECT > ", columns, where_expr, where_vals, additional_query);
      items = await this.table.select(columns, where_expr, where_vals, additional_query);
    } else {
      if (typeof columns != "string") columns = columns.join(", ");
      console.log(`SELECT ${columns} FROM ${this.table.name} ${additional_query}`);
      let qstr = `SELECT ${columns} FROM ${this.table.name} ${additional_query}`;
      items = (await this.pg.query(qstr)).rows;
    }

    for (let item of items) {
      for (let key in item) {
        const col = this.get_column(key);
        if (key !== "id" && col && col.type == "text" && typeof item[key] == "string") {
          item[key] = item[key].replace(/&#39;/gm, "'");
        }
      }
    }


    return items;
  }

  async insert(columns, expressions, values) {


    const return_values = {};


    for (let colobj of this.columns) {
      if (
        (
          colobj.type == "int" ||
          colobj.type == "float" ||
          colobj.type == "double"
        ) &&
        typeof colobj.default == "number" &&
        !columns.includes(colobj.name)
      ) {
        columns.push(colobj.name);
        expressions.push(`$${values.length+1}`)
        values.push(colobj.default);
      } else if (colobj.type == "text" && colobj.random) {
        const rstr = StringUtil.random(colobj.random.length, rand_charset);
        
        columns.push(colobj.name);
        expressions.push(`$${values.length+1}`)
        values.push(rstr);
        return_values[colobj.name] = rstr;
      } else if (colobj.type == "date-time" && (
        colobj.auto == "created" ||
        colobj.auto == "edited"
      )) {
        const date_time = new Date().toISOString().slice(0, 19).replace('T', ' ');
        columns.push(colobj.name);
        expressions.push(`$${values.length+1}`)
        values.push(date_time);
        return_values[colobj.name] = date_time;
      }
    }

    let new_id = await this.table.insert(columns, expressions, values, "returning id");
    if (typeof new_id.id == "number") new_id.id = new_id.id.toString();
    return_values.id = new_id.id;
    return return_values;
  }

  allowed(req) {
    if (this.permit) {
      for (let permit of this.permit) {
        if (req.flyauth.privileges && req.flyauth.privileges.includes(permit)) return true;
      }
    } else {
      return true;
    }
    return false;
  }

  async count() {
    try {
      return (await this.table.aura.query(`SELECT COUNT(*) FROM ${this.table.name};`)).rows[0].count;
    } catch (e) {
      console.error(e.stack);
    }
  }


}
